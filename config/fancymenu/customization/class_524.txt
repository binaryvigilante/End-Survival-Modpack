type = menu

customization-meta {
  identifier = net.minecraft.class_524
  renderorder = foreground
}

customization {
  path = modpack-graphics/modpack-screen-worlds.png
  action = texturizebackground
}

customization {
  identifier = %id=400279%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=400327%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=400375%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=502399%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-120-20-pressed.png
  backgroundnormal = modpack-graphics/button-120-20.png
}

customization {
  identifier = %id=400255%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=400303%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=400351%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  backgroundnormal = modpack-graphics/button-128-20.png
}

customization {
  identifier = %id=400399%
  action = setbuttontexture
  backgroundhovered = modpack-graphics/button-120-20-pressed.png
  backgroundnormal = modpack-graphics/button-120-20.png
}

