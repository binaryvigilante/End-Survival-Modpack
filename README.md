# End Survival Modpack
My End Survival modpack for surviving exclusively in the end dimension!

## Installation Guide

1. Download the modpack from my website (https://binaryvigilante.com/downloads/end-survival-modpack/)
2. Install the latest version of the Fabric Loader for Minecraft 1.17.1
3. Extract the `mods` folder, `configs` folder and `modpack-graphics` folder to your `.minecraft` folder
4. Done :)

### If you are using MultiMC
1. Download the modpack: https://binaryvigilante.com/downloads/end-survival-modpack/
2. Create a new instance
3. Select "Import from zip"
4. Click "Browse" and select the downloaded zip file
5. Click "OK"
6. Done :)
