type = menu

customization-meta {
  identifier = Modpack_Menu
  renderorder = foreground
}

customization {
  path = modpack-graphics/modpack-screen.png
  action = texturizebackground
}

customization {
  orientation = mid-left
  buttonaction = opengui
  x = 0
  width = 100
  actionid = c5d90201-01c6-424f-ab6f-09e6ffc9de331622677225277
  action = addbutton
  y = -10
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Multiplayer
  backgroundnormal = modpack-graphics/button-100-20.png
  value = net.minecraft.class_500
  height = 20
}

customization {
  orientation = mid-left
  buttonaction = opengui
  x = 0
  width = 100
  actionid = 33a8b29b-6dbd-4bbc-8f2d-91822683e04a1622677225287
  action = addbutton
  y = 32
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Mods
  backgroundnormal = modpack-graphics/button-100-20.png
  value = com.terraformersmc.modmenu.gui.ModsScreen
  height = 20
}

customization {
  orientation = mid-left
  buttonaction = opengui
  x = 0
  width = 100
  actionid = 656c7992-4616-4ffc-890e-817c23102d1b1622677225288
  action = addbutton
  y = -31
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Worlds
  backgroundnormal = modpack-graphics/button-100-20.png
  value = net.minecraft.class_526
  height = 20
}

customization {
  orientation = bottom-centered
  buttonaction = quitgame
  x = -64
  width = 128
  actionid = ba79400f-4fcc-4a22-9848-136084da3c081622677225288
  action = addbutton
  y = -20
  backgroundhovered = modpack-graphics/button-128-20-pressed.png
  label = Quit (give up)
  backgroundnormal = modpack-graphics/button-128-20.png
  value = menu.quit
  height = 20
}

customization {
  orientation = bottom-left
  buttonaction = opengui
  x = 0
  width = 56
  actionid = a33ec690-23ff-44cf-86ba-a4c62ad2c6ff1622677225288
  action = addbutton
  y = -20
  backgroundhovered = modpack-graphics/56-20-pressed.png
  label = Language
  backgroundnormal = modpack-graphics/56-20.png
  value = net.minecraft.class_426
  height = 20
}

customization {
  orientation = mid-right
  shadow = true
  x = -86
  actionid = 85ec44ae-dd8d-4c1a-9f9f-e4b39ec6c41b1622677225288
  action = addtext
  y = -41
  scale = 1.0
  alignment = left
  value = BinaryVigilante:
}

customization {
  orientation = mid-right
  buttonaction = openlink
  x = -100
  width = 100
  actionid = a732485e-465f-4519-b2a7-df7b364481e71622677225290
  action = addbutton
  y = -31
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = YouTube
  backgroundnormal = modpack-graphics/button-100-20.png
  value = https://www.youtube.com/binaryVigilante?sub_confirmation=1
  height = 20
}

customization {
  orientation = mid-right
  buttonaction = openlink
  x = -100
  width = 100
  actionid = 4c735712-feba-466a-ba32-f293a848fcd51622677225290
  action = addbutton
  y = 11
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Twitter
  backgroundnormal = modpack-graphics/button-100-20.png
  value = https://twitter.com/BinaryVigilante
  height = 20
}

customization {
  orientation = mid-right
  buttonaction = openlink
  x = -100
  width = 100
  actionid = 72e5da26-5425-4c4a-a97f-4ffe88814cb61622677225290
  action = addbutton
  y = -10
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Patreon
  backgroundnormal = modpack-graphics/button-100-20.png
  value = https://www.patreon.com/BinaryVigilante
  height = 20
}

customization {
  orientation = mid-right
  buttonaction = openlink
  x = -100
  width = 100
  actionid = 16ae5e36-890f-44e1-a475-1cd5a355c06f1622677225290
  action = addbutton
  y = 32
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Discord
  backgroundnormal = modpack-graphics/button-100-20.png
  value = 
  height = 20
}

customization {
  orientation = mid-left
  buttonaction = opengui
  x = 0
  width = 100
  actionid = 53796cd6-efb9-451c-a1f0-59f1c4a93bea1622677225290
  action = addbutton
  y = 11
  backgroundhovered = modpack-graphics/button-100-20-pressed.png
  label = Settings
  backgroundnormal = modpack-graphics/button-100-20.png
  value = net.minecraft.class_429
  height = 20
}

